package zut.wi.wpardel;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class FTP 
{
	Socket socket;
	Socket dataSocket;
	
	BufferedReader reader;
	BufferedWriter writer;
	
	BufferedReader dataReader;
	BufferedWriter dataWriter;
	
	//private String HOST = "files.000webhost.com";
	private String HOST;
	private String DATA_HOST;
	private int COMMAND_PORT ;
	private int DATA_PORT;
	private String userName ;
	private String passwd;
		
		
	
	// Connect to ROOT
	public FTP()
	{
		try
		{
			this.LoadSettingsFromXML();
			Connect();
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			System.out.println("FTP = " + ReadAllResponseFromServer());			
			authorizeCMD();			
			awaitDecision();
			//enterPassiveMode();
			//listFTWCommand(-1,"/"); // -1 aby poprawnie tabulowa� 
		} 
		catch(Exception e )
		{
			e.printStackTrace();
			return ;
		}
	}
	public void menu()
	{
		// Wy�wietl menu nawigacyjne
		System.out.println();
		System.out.println("===============================================");
		System.out.println("================FTP CLIENT MENU================");
		System.out.println("===============================================");
		System.out.println("1.Obecna Lokalizacja -> PWD");
		System.out.println("2.Nast�pny katalog <Nazwa katalogu> -> CWD");
		System.out.println("3.Wypisz drzewo -> FTW ");
		System.out.println("4.Katalog wy�ej -> CDUP");
		System.out.println("5.Obecny katalog + lista zawarto�ci -> PWD/LIST");		
		System.out.println("6.Zako�cz program");
		System.out.println("===============================================");		
		System.out.println("===============================================");
		System.out.println("===============================================");
		System.out.println();		
		System.out.print("Wybierz Opcj�: ");
	}
	public void awaitDecision()
	{
		while(true)
		{
			boolean flagExit = false;
			menu();
			
			int Option;
			Scanner numberIn = new Scanner(System.in);
			Option = numberIn.nextInt();
			switch(Option)
			{
				case 1:
					System.out.println("PWD =" + pwdCommand());
					break;
				case 2:
					System.out.print("Podaj katalog: ");
					String targetDir = numberIn.next();
					System.out.println(cwdCommand(targetDir));
					break;
				case 3:
					listFTWCommand(-1, "/");
					break;
				case 4:
					System.out.println(cwdUpCommand());
					break;
				case 5:
					listTargetCommand("");
					break;
				case 6:
					flagExit = true;
					break;
			
			}
			if(flagExit)break;
		}
	}
	// Connect to any folder
	public FTP(String Dir)
	{
		try
		{
			Connect();
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
			
		} 
		catch (IOException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void LoadSettingsFromXML()
	{
			
		try
		{
			File file = new File("Options.xml");
			DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory
			        .newInstance();
			DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
			Document document = documentBuilder.parse(file);
			this.HOST = document.getElementsByTagName("Server").item(0).getTextContent();
			this.COMMAND_PORT = Integer.valueOf(document.getElementsByTagName("Port").item(0).getTextContent());
			this.userName = document.getElementsByTagName("Username").item(0).getTextContent(); 
			this.passwd = document.getElementsByTagName("Pass").item(0).getTextContent();
			

			int t =5;
		} 
		catch (SAXException e)
		{			
			e.printStackTrace();
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
		}
		catch (ParserConfigurationException e)
		{
			e.printStackTrace();
		}
	}
	
	public boolean authorizeCMD()
	{
		System.out.println("USER " + userName);
		WriteToServer("USER "+userName+"\r\n");
		System.out.println(ReadAllResponseFromServer());
		
		System.out.println("PASS " + passwd);
		WriteToServer("PASS "+passwd+"\r\n");
		System.out.println(ReadAllResponseFromServer());
		return true;
	}

	public boolean enterPassiveMode(boolean devlog)
	{
		//System.out.println("PASV");
		WriteToServer("PASV\r\n");		
		String Response = ReadAllResponseFromServer();
		
		if(devlog)System.out.println("DEV RESPONSE = " + Response);
		Pattern pat = Pattern.compile("([\\d]+)");
		Matcher m = pat.matcher(Response);
		List<String> numbers = new ArrayList<String>();
		m.matches();
		
		while(m.find())
		{			
			numbers.add(m.group(1));
		}	
		
		this.DATA_PORT = calculatePassvPortNumber(Integer.parseInt(numbers.get(4)), Integer.parseInt(numbers.get(5)));
		this.DATA_HOST = prepareIpAdress(numbers.get(0), numbers.get(1), numbers.get(2), numbers.get(3));	
		if(devlog)System.out.println("IP = " + this.DATA_HOST);
		if(devlog)System.out.println("PORT = " + this.DATA_PORT);
		ConnectToPASV();
		return true;
	}
	
	public String prepareIpAdress(String num1,String num2,String num3,String num4)
	{
		String ip=num1+"."+num2+"."+num3+"."+num4;
		return ip;
	}
	
	public int calculatePassvPortNumber(int num1 , int num2)
	{		
		return (num1 * 256) + num2;
	}
		
	public boolean Connect()
	{
		try
		{
			socket = new Socket(HOST, COMMAND_PORT);
			
		} 
		catch (UnknownHostException e) 
		{
			e.printStackTrace();
			return false;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public boolean ConnectToPASV()
	{
		try
		{
			this.dataSocket = new Socket(DATA_HOST, DATA_PORT);
			//dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
			//dataWriter = new BufferedWriter(new OutputStreamWriter(dataSocket.getOutputStream()));
			
			return true;
			
		}
		catch(UnknownHostException e)
		{
			e.printStackTrace();
			return false;
		} 
		catch (IOException e) 
		{			
			e.printStackTrace();
			return false;
		}
	}
		
	public boolean Disconnect()
	{
		return true;
	}
	
	public String pwdCommand()
	{
		WriteToServer("PWD\r\n");
		return ReadAllResponseFromServer();
	}
	
	public String cwdCommand(String targetDir)
	{
		WriteToServer("CWD " + targetDir + "\r\n");
		return ReadAllResponseFromServer();
	}
	
	public String cwdUpCommand()
	{
		WriteToServer("CDUP\r\n");
		return ReadAllResponseFromServer();
	}
	
	public void listTargetCommand(String dir)
	{
		if(dir.equals(""))
		{
			// To znaczy obecny ( tam gdzie wskazuje PWD)
			WriteToServer("PWD\r\n");
			System.out.println("PWD = "+ReadAllResponseFromServer().split("\""));
			enterPassiveMode(false);
			WriteToServer("LIST\r\n");
			ReadAllResponseFromServer();
			System.out.println(ReadAllResponseFromServerData());
			
		}
		else
		{
			// Wypisz zawarto�c katalogu zawatego w scie�ce 
		}
	}

	public void listFTWCommand(int deep , String dir )
	{
		//ReadAllResponseFromServer();
		
		enterPassiveMode(false);
		deep ++;
		String lookUpDir = dir;
		//System.out.println("LOOK UP = " + lookUpDir);
		String tabSpace="";
		for(int i = 0;i<deep;i++)
		{
			tabSpace += "\t";
		}
		
		WriteToServer("LIST " + lookUpDir + "\r\n"  );
		try 
		{
			Thread.sleep(1000);
		} 
		catch (InterruptedException e) 
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		ReadAllResponseFromServer();
		String [] Response = ReadAllResponseFromServerData().split("\n");
				
		for(String object : Response)
		{
			String [] entity = object.split(" ");
			// Nie wy�wietlamy . i ..
			if(!entity[entity.length-1].equals(".") && !entity[entity.length-1].equals(".."))
			{
				System.out.println(tabSpace + entity[0] +"  " + entity[entity.length - 1]);
			}
			// To katalog rekurencja Yay 
			if(entity[0].startsWith("d") && !entity[entity.length-1].equals(".") && !entity[entity.length-1].equals("..")) 
			{
				listFTWCommand(deep,lookUpDir + entity[entity.length-1]+"/");
			}
		}
	}
	
	public String helpCommand()
	{
		WriteToServer("HELP\r\n");
		return ReadAllResponseFromServer();
	}
	
	
	public void WriteToServer(String str)
	{
		if(socket.isConnected())
		{
			try 
			{
				//writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				writer.write(str);
				writer.flush();				
				
			}
			catch (IOException e) 
			{				
				e.printStackTrace();
			}
		}		
	}
	
	public String ReadAllResponseFromServer()
	{		
		if(socket.isConnected())
		{
			String response="";
			try 
			{
				//reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				
				while(!reader.ready()){}
				while( true)
				{
					String line = reader.readLine();					
					 
		            if(line.isEmpty() || line == null){System.out.println("LINE IS EMPTY"); break;}
		            response += line+"\n" ;
		            line = null;
		            //System.out.println("WHILE IS LOOPING");
		            if(!reader.ready()) break;       
		          
				
				}
				//System.out.println("OUT OF LOOP");
				
				return response;
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				System.out.println("IO EXCEPTION");
				e.printStackTrace();
				return null;
			}
			//System.out.println("SERVER RESPONDED WITH = " + response);


						
		}
		return null;		
	}
	
	public void WriteToServerData(String str)
	{
		if(dataSocket.isConnected())
		{
			try 
			{
				//writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
				dataWriter = new BufferedWriter(new OutputStreamWriter(dataSocket.getOutputStream()));
				dataWriter.write(str);
				dataWriter.flush();				
				
			}
			catch (IOException e) 
			{				
				e.printStackTrace();
			}
		}	
	}
	
	public String ReadAllResponseFromServerData()
	{
		if(dataSocket.isConnected())
		{
			String response="";
			try 
			{
				//reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
				dataReader = new BufferedReader(new InputStreamReader(dataSocket.getInputStream()));
				
				while(!dataReader.ready()){}
				while( true)
				{
					String line = dataReader.readLine();					
					 
		            if(line.isEmpty() || line == null){System.out.println("LINE IS EMPTY"); break;}
		            response += line+"\n" ;
		            line = null;
		            //System.out.println("WHILE IS LOOPING");
		            if(!dataReader.ready()) break;       
		          
				
				}
				//System.out.println("OUT OF LOOP");
				
				return response;
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				System.out.println("IO EXCEPTION");
				e.printStackTrace();
				return null;
			}
			//System.out.println("SERVER RESPONDED WITH = " + response);						
		}
		return null;	
		
		
	}

}
